
class Parent {
	foo(a) {
		return a;
	}
}

class Child extends Parent {
	bar() {
		return "text about nothing";
	}
}

export default Child;
